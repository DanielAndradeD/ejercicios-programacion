/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int aleatorio=0;
    int numero=0;
    int contador=5;
    int bandera=0;
    srand(time(NULL));
    aleatorio=rand()%4;
    printf("Adivina el numero entre el 0 y 100\n");
    do{
        printf("Ingrese el numero\n");
        scanf("%d",&numero);
        if(numero==aleatorio){
            printf("Ganaste!!! ");
            bandera=-1;
        }
        else{
            printf("Sigue intentandolo, te quedan %i intentos\n", contador-1);
            contador--;
        }
        
        
    }while(contador!=0 && bandera!=-1);
    if(contador==0){
        printf("Perdiste!!! ");
    }
    printf("El numero era %i", aleatorio);
    return 0;
}