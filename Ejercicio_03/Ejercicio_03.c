/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    const float valor_peso=22.37;
    float conversion=0;
    int dolares=0;
    printf("Ingrese la cantidad de dolares que desea convertir:\n");
    scanf("%d",&dolares);
    conversion=dolares*valor_peso;
    printf("\nEl total en pesos mexicanos es: %f pesos",conversion);
    
    return 0;
}
