/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float lados_iguales=0;
    float tercer_lado=0;
    float perimetro=0;
    printf("Ingrese la longitud de los lados iguales del triangulo: ");
    scanf("%f",&lados_iguales);
    printf("Ingrese la longitud del tercer lado: ");
    scanf("%f",&tercer_lado);
    perimetro=(lados_iguales*2)+tercer_lado;
    printf("\nEl perimetro del triangulo es: %f",perimetro);
    return 0;
}
