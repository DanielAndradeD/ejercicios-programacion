/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <string.h>

int main()
{
    int mes=0;
    char nombre[20]=" ";
    printf("Ingrese el numero del mes, tomando en cuenta que Enero es 0 y Diciembre es 11: ");
    scanf("%d",&mes);
    switch(mes){
        case 0: 
            strcpy( nombre, "Enero" );
            break;
        case 1: 
            strcpy( nombre, "Febrero" );
            break;
        case 2:
            strcpy( nombre, "Marzo" );
            break;
        case 3:
            strcpy( nombre, "Abril" );
            break;
        case 4:
            strcpy( nombre, "Mayo" );
            break;
        case 5:
            strcpy( nombre, "Junio" );
            break;
        case 6:
            strcpy( nombre, "Julio" );
            break;
        case 7:
            strcpy( nombre, "Agosto" );
            break;
        case 8:
            strcpy( nombre, "Septiembre" );
            break;
        case 9:
            strcpy( nombre, "Octubre" );
            break;
        case 10:
            strcpy( nombre, "Noviembre" );
            break;
        case 11:
            strcpy( nombre, "Diciembre" );
            break;
        default:
            strcpy( nombre, "Numero invalido" );
    }
    printf("El mes es: %s",nombre);
    return 0;
}
