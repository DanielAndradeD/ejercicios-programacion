/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int apostar();
int volado();

int main()
{
    int cartera_usuario=500;
    int cartera_maquina=500;
    int apuesta_usuario=0;
    int apuesta_maquina=0;
    int seleccion=0;
    int contador=3;
    int volado2=0;
    int victorias_usuario=0;
    int victorias_maquina=0;
    srand(time(NULL));
    while(contador!=0){
        do{
            printf("Ingrese la cantidad que desea apostar entre 20 y 100 ");
            scanf("%d",&apuesta_usuario);
            if(apuesta_usuario<20 ||apuesta_usuario>100){
                printf("Apuesta no valida, intentelo de nuevo\n ");
            }
        }while(apuesta_usuario<20 ||apuesta_usuario>100);
        apuesta_maquina=apostar();
        do{
            printf("Escoge: si quiere aguila, ingrese 1, si quiere sol, ingrese un 2 ");
            scanf("%d",&seleccion);
            if(seleccion<1 || seleccion>2){
                printf("Valor ingresado no valido, intentelo de nuevo\n ");
            }
        }while(seleccion<1 || seleccion>2);
        volado2=volado();
        if(volado2==seleccion){
            printf("Ganaste esta ronda\n");
            cartera_usuario=cartera_usuario+apuesta_maquina;
            cartera_maquina=cartera_maquina-apuesta_maquina;
            victorias_usuario++;
        }else{
            printf("Perdiste esta ronda\n");
            cartera_usuario=cartera_usuario - apuesta_usuario;
            cartera_maquina=cartera_maquina + apuesta_usuario;
            victorias_maquina++;
        }
        contador--;
    }
    if(victorias_usuario>victorias_maquina){
        printf("Ganaste más partidas :D\n");
    }
    else{
        printf("La maquina ganó mas partidas :(\n");
    }
    printf("Cantdad total de dinero de la maquina: %i\n", cartera_maquina);
    printf("cantidad total de dinero del usuario: %i", cartera_usuario);
    return 0;
}

int apostar(){
    int aleatorio=0;
    aleatorio=20 + rand()%(101 - 20);
}

int volado(){
    int volado=0;
    volado= 1 + rand()%(3-1);
}