/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    int entero=0;
    int ping=0;
    int pong=0;
    do{
        ping=entero%3;
        pong=entero%5;
        if(ping==0 && pong==0){
            printf( "%i Ping-Pong\n",entero);
        }
        else if(ping == 0){
            printf("%i Ping\n",entero);
        }
        else if(pong==0){
            printf("%i Pong\n", entero);
        }
        entero++;
    }while(entero<101);
    
    return 0;
}
   
    