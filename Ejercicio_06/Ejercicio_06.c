/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float lado_a=0;
    float lado_b=0;
    float lado_c=0;
    float perimetro=0;
    printf("Ingrese la longitud del lado a: ");
    scanf("%f",&lado_a);
    printf("Ingrese la longitud del lado b: ");
    scanf("%f",&lado_b);
    printf("Ingrese la longitud del lado c: ");
    scanf("%f",&lado_c);
    perimetro=lado_a+lado_b+lado_c;
    printf("\nEl perimetro del triangulo es: %f",perimetro);
    return 0;
}
