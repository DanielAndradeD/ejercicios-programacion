/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    int suma=0;
    int numero=0;
    int contador;
    printf("ingrese un numero entre 1 y 50: \n");
    do{
        scanf("%d",&numero);
        if(numero > 50){
            printf("Numero no valido, intentelo de nuevo \n");
        }
    }while(numero>50);
    for(contador=0; contador<numero; contador++){
        suma=suma+(contador+1);
    }
    printf("\n La suma total es %i",suma);
    return 0;
}
