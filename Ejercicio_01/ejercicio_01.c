/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    int entero=0;
    int resultado=0;
    do{
        resultado=entero%2;
        if(resultado == 0){
            printf( "%i es numero par \n",entero);
        }
        entero++;
    }while(entero<101);
    
    return 0;
}
