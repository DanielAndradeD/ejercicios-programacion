/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    float lado=0;
    float perimetro=0;
    printf("Ingrese la longitud de los lados del triangulo equilatero: ");
    scanf("%f",&lado);
    perimetro=lado*3;
    printf("\nEl perimetro del triangulo es: %f",perimetro);
    return 0;
}
